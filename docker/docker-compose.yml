version: "2.1"

services:
  # For Celery
  amqp:
    image: rabbitmq:3.6-management
    ports:
      - 5072:5672

  # Kafka related
  kafka:
    image: docker.io/confluentinc/confluent-local
    environment:
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://:9092,PLAINTEXT_HOST://:9092
      KAFKA_NUM_PARTITIONS: 16
      KAFKA_LOG_CLEANUP_POLICY: compact

  kafka-create-topics:
    # this is a short-lived service, it creates kafka topics and exits
    image: swh/stack
    depends_on:
      - kafka
    restart: on-failure
    command: wait-for-it kafka:9092 -s --timeout=0 -- python /create_topics.py
    volumes:
      - "./utils/create_topics.py:/create_topics.py:ro"

  # monitoring
  prometheus:
    image: prom/prometheus
    depends_on:
      - prometheus-statsd-exporter
    command:
      # Needed for the reverse-proxy
      - "--web.external-url=/prometheus"
      - "--config.file=/etc/prometheus/prometheus.yml"
    volumes:
      - "./conf/prometheus.yml:/etc/prometheus/prometheus.yml:ro"
    restart: unless-stopped

  prometheus-statsd-exporter:
    image: prom/statsd-exporter
    command:
      - "--statsd.mapping-config=/etc/prometheus/statsd-mapping.yml"
    volumes:
      - "./conf/prometheus-statsd-mapping.yml:/etc/prometheus/statsd-mapping.yml:ro"
    restart: unless-stopped

  prometheus-rabbitmq-exporter:
    image: kbudde/rabbitmq-exporter
    restart: unless-stopped
    environment:
      SKIP_QUEUES: "RPC_.*"
      MAX_QUEUES: 5000
      RABBIT_URL: http://amqp:15672
      LOG_LEVEL: warning

  grafana:
    image: grafana/grafana
    restart: unless-stopped
    depends_on:
      - prometheus
    environment:
      GF_SERVER_ROOT_URL: http://localhost:5080/grafana
    volumes:
      - "./conf/grafana/provisioning:/etc/grafana/provisioning:ro"
      - "./conf/grafana/dashboards:/var/lib/grafana/dashboards"

  # cache
  memcache:
    image: memcached
    restart: unless-stopped

  redis:
    image: redis
    volumes:
      - redis-data:/data
    ports:
      - 6379
    command:
      - "--save"
      - "60"
      - "1" # flush every minutes
    healthcheck:
      test: [ "CMD", "redis-cli", "PING" ]
      interval: 1s
      timeout: 5s
      retries: 10

  # main web reverse proxy for many services
  nginx:
    image: nginx
    volumes:
      - "./conf/nginx.conf:/etc/nginx/nginx.conf:ro"
    ports:
      - 5080:5080

  # SMTP service (used by keycloak and vault)
  mailhog:
    image: mailhog/mailhog


  # Scheduler

  swh-scheduler-db:
    image: postgres:16
    environment:
      POSTGRES_DB: swh-scheduler
      POSTGRES_PASSWORD: testpassword
    volumes:
      - "./services/initdb.d:/docker-entrypoint-initdb.d"

  swh-scheduler:
    image: swh/stack
    build: ./
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-scheduler
      SWH_CONFIG_FILENAME: /scheduler.yml
      SWH_SCHEDULER_CONFIG_FILE: /scheduler.yml
      RPC_PORT: 5008
    entrypoint: /entrypoint.sh
    command: rpc
    healthcheck:
      test: curl -f http://localhost:5008 || exit 1
      retries: 6
    depends_on:
      - swh-scheduler-db
      - prometheus-statsd-exporter
    ports:
      - 5008:5008
    volumes:
      - "./conf/scheduler.yml:/scheduler.yml:ro"
      - "./services/swh-scheduler/entrypoint.sh:/entrypoint.sh:ro"

  swh-scheduler-listener:
    image: swh/stack
    build: ./
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-scheduler
      SWH_CONFIG_FILENAME: /scheduler.yml
      SWH_SCHEDULER_CONFIG_FILE: /scheduler.yml
      BROKER_URL: amqp://amqp/
      WORKER_INSTANCES: listers, loader
      APP: swh.scheduler.celery_backend.config.app
    entrypoint: /entrypoint.sh
    command: worker start-listener
    depends_on:
      - swh-scheduler
      - amqp
      - prometheus-statsd-exporter
    volumes:
      - "./conf/scheduler.yml:/scheduler.yml:ro"
      - "./services/swh-scheduler/entrypoint.sh:/entrypoint.sh:ro"

  swh-scheduler-runner:
    image: swh/stack
    build: ./
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-scheduler
      SWH_CONFIG_FILENAME: /scheduler.yml
      SWH_SCHEDULER_CONFIG_FILE: /scheduler.yml
      BROKER_URL: amqp://amqp/
      WORKER_INSTANCES: listers, loader
      APP: swh.scheduler.celery_backend.config.app
    entrypoint: /entrypoint.sh
    command: worker start-runner --period 10
    depends_on:
      - swh-scheduler
      - amqp
      - prometheus-statsd-exporter
    volumes:
      - "./conf/scheduler.yml:/scheduler.yml:ro"
      - "./services/swh-scheduler/entrypoint.sh:/entrypoint.sh:ro"

  swh-scheduler-runner-priority:
    image: swh/stack
    build: ./
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-scheduler
      SWH_CONFIG_FILENAME: /scheduler.yml
      SWH_SCHEDULER_CONFIG_FILE: /scheduler.yml
      BROKER_URL: amqp://amqp/
      WORKER_INSTANCES: listers, loader
      APP: swh.scheduler.celery_backend.config.app
    entrypoint: /entrypoint.sh
    command: worker start-runner --period 10 --with-priority
    depends_on:
      - swh-scheduler
      - amqp
      - prometheus-statsd-exporter
    volumes:
      - "./conf/scheduler.yml:/scheduler.yml:ro"
      - "./services/swh-scheduler/entrypoint.sh:/entrypoint.sh:ro"

  swh-scheduler-schedule-recurrent:
    image: swh/stack
    build: ./
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-scheduler
      SWH_CONFIG_FILENAME: /scheduler.yml
      SWH_SCHEDULER_CONFIG_FILE: /scheduler.yml
      BROKER_URL: amqp://amqp/
      WORKER_INSTANCES: listers, loader
      APP: swh.scheduler.celery_backend.config.app
    entrypoint: /entrypoint.sh
    command: worker schedule-recurrent
    depends_on:
      - swh-scheduler
      - swh-loader
      - amqp
      - prometheus-statsd-exporter
    volumes:
      - "./conf/scheduler.yml:/scheduler.yml:ro"
      - "./services/swh-scheduler/entrypoint.sh:/entrypoint.sh:ro"

  swh-scheduler-update-metrics:
    image: swh/stack
    build: ./
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-scheduler
      SWH_CONFIG_FILENAME: /scheduler.yml
      SWH_SCHEDULER_CONFIG_FILE: /scheduler.yml
    entrypoint: /entrypoint.sh
    command: update-metrics
    depends_on:
      - swh-scheduler-db
    volumes:
      - "./conf/scheduler.yml:/scheduler.yml:ro"
      - "./services/swh-scheduler/entrypoint.sh:/entrypoint.sh:ro"

  # Graph storage

  swh-storage-db:
    image: postgres:16
    environment:
      POSTGRES_DB: swh-storage
      POSTGRES_PASSWORD: testpassword
    volumes:
      - "./services/initdb.d:/docker-entrypoint-initdb.d"
      - storage-data:/var/lib/postgresql

  swh-storage:
    image: swh/stack
    build: ./
    ports:
      - 5002:5002
    depends_on:
      - swh-storage-db
      - swh-objstorage
      - kafka-create-topics
      - nginx
      - prometheus-statsd-exporter
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-storage
      SWH_CONFIG_FILENAME: /storage.yml
      RPC_PORT: 5002
    entrypoint: /entrypoint.sh
    command: rpc
    healthcheck:
      test: curl -f http://localhost:5002 || exit 1
      retries: 6
    volumes:
      - "./conf/storage.yml:/storage.yml:ro"
      - "./services/swh-storage/entrypoint.sh:/entrypoint.sh:ro"

  # Object storage

  swh-objstorage:
    build: ./
    image: swh/stack
    ports:
      - 5003:5003
    depends_on:
      - prometheus-statsd-exporter
    env_file:
      - ./env/common_python.env
    environment:
      SWH_CONFIG_FILENAME: /objstorage.yml
      RPC_PORT: 5003
    entrypoint: /entrypoint.sh
    command: rpc
    healthcheck:
      test: curl -f http://localhost:5003 || exit 1
      retries: 6
    volumes:
      - "./conf/objstorage.yml:/objstorage.yml:ro"
      - "./services/swh-objstorage/entrypoint.sh:/entrypoint.sh:ro"
      - objstorage-data:/srv/softwareheritage/objects

  # Indexer storage

  swh-idx-storage-db:
    image: postgres:16
    environment:
      POSTGRES_DB: swh-idx-storage
      POSTGRES_PASSWORD: testpassword
    volumes:
      - "./services/initdb.d:/docker-entrypoint-initdb.d"

  swh-idx-storage:
    image: swh/stack
    build: ./
    ports:
      - 5007:5007
    depends_on:
      - swh-idx-storage-db
      - prometheus-statsd-exporter
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-idx-storage
      SWH_CONFIG_FILENAME: /indexer_storage.yml
      RPC_PORT: 5007
    entrypoint: /entrypoint.sh
    command: rpc
    healthcheck:
      test: curl -f http://localhost:5007 || exit 1
      retries: 6
    volumes:
      - "./conf/indexer_storage.yml:/indexer_storage.yml:ro"
      - "./services/swh-indexer/entrypoint.sh:/entrypoint.sh:ro"

  # Web interface
  swh-web-db:
    image: postgres:16
    environment:
      POSTGRES_DB: swh-web
      POSTGRES_PASSWORD: testpassword
    volumes:
      - "./services/initdb.d:/docker-entrypoint-initdb.d"

  swh-web:
    build: ./
    image: swh/stack
    ports:
      - 3000:3000
      - 5004:5004
    depends_on:
      - swh-idx-storage
      - swh-scheduler
      - swh-storage
      - swh-web-db
      - swh-search
      - memcache
      - prometheus-statsd-exporter
      - nginx
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-web
      VERBOSITY: 3
      DJANGO_SETTINGS_MODULE: swh.web.settings.production
      SWH_CONFIG_FILENAME: /web.yml
    entrypoint: /entrypoint.sh
    healthcheck:
      test: curl -f http://localhost:5004 || exit 1
      retries: 6
    volumes:
      - "./conf/web.yml:/web.yml:ro"
      - "./services/swh-web/entrypoint.sh:/entrypoint.sh:ro"

  swh-web-cron:
    build: ./
    image: swh/stack
    depends_on:
      - swh-web
      - nginx
    env_file:
      - ./env/common_python.env
    environment:
      POSTGRES_DB: swh-web
      VERBOSITY: 2
      DJANGO_SETTINGS_MODULE: swh.web.settings.production
      SWH_CONFIG_FILENAME: /web.yml
    entrypoint: /entrypoint.sh
    command: cron
    volumes:
      - "./conf/web.yml:/web.yml:ro"
      - "./services/swh-web/entrypoint.sh:/entrypoint.sh:ro"

  # Lister Celery workers

  swh-lister:
    image: swh/stack
    build: ./
    env_file:
      - ./env/common_python.env
      - ./env/listers.env
      - ./env/workers.env
    user: swh
    environment:
      SWH_WORKER_INSTANCE: listers
      SWH_CONFIG_FILENAME: /lister.yml
      SWH_SCHEDULER_INSTANCE: http://nginx:5080/scheduler
    depends_on:
      - swh-scheduler
      - swh-scheduler-runner
      - amqp
      - swh-lister-maven-nginx
      - prometheus-statsd-exporter
      - nginx
    entrypoint: /entrypoint.sh
    command: worker
    volumes:
      - "./conf/lister.yml:/lister.yml:ro"
      - "./services/swh-worker/entrypoint.sh:/entrypoint.sh:ro"


  swh-lister-maven-nginx:
    # Http server to host the maven extracted index for the maven lister
    image: nginx
    volumes:
      - "./conf/maven_index/:/usr/share/nginx/html:ro"
    ports:
      - 8880:80

  # Loader + deposit checker Celery workers

  swh-loader:
    image: swh/stack
    build: ./
    deploy:
      replicas: 4
    env_file:
      - ./env/common_python.env
      - ./env/workers.env
    user: swh
    environment:
      SWH_WORKER_INSTANCE: loader
      SWH_CONFIG_FILENAME: /loader.yml
      SWH_SCHEDULER_INSTANCE: http://nginx:5080/scheduler
    entrypoint: /entrypoint.sh
    command: worker
    depends_on:
      - swh-storage
      - swh-scheduler
      - amqp
      - nginx
      - prometheus-statsd-exporter
    volumes:
      - "./conf/loader.yml:/loader.yml:ro"
      - "./services/swh-worker/entrypoint.sh:/entrypoint.sh:ro"

  # Indexer workers

  swh-indexer-worker-journal:
    image: swh/stack
    build: ./
    user: swh
    env_file:
      - ./env/common_python.env
      - ./env/workers.env
    environment:
      SWH_WORKER_INSTANCE: indexer
      SWH_CONFIG_FILENAME: /indexer.yml
      CONCURRENCY: 4
      POSTGRES_DB: swh-idx-storage
      SWH_SCHEDULER_INSTANCE: http://nginx:5080/scheduler
    entrypoint: /entrypoint.sh
    command: journal-client
    depends_on:
      - kafka
      - swh-storage
      - swh-idx-storage
      - swh-objstorage
      - nginx
    volumes:
      - "./conf/indexer.yml:/indexer.yml:ro"
      - "./services/swh-indexer/entrypoint.sh:/entrypoint.sh:ro"

  # Journal related

  swh-scheduler-journal-client:
    image: swh/stack
    build: ./
    entrypoint: /entrypoint.sh
    command: journal-client
    environment:
      SWH_CONFIG_FILENAME: /scheduler.yml
    env_file:
      - ./env/common_python.env
    depends_on:
      - kafka
      - swh-scheduler
      - nginx
    volumes:
      - "./conf/scheduler_journal_client.yml:/scheduler.yml:ro"
      - "./services/swh-scheduler/entrypoint.sh:/entrypoint.sh:ro"

  swh-counters:
    image: swh/stack
    build: ./
    entrypoint: /entrypoint.sh
    command: rpc
    environment:
      SWH_CONFIG_FILENAME: /counters.yml
      RPC_PORT: 5011
    env_file:
      - ./env/common_python.env
    ports:
      - 5011:5011
    depends_on:
      - redis
      - prometheus-statsd-exporter
    volumes:
      - "./conf/counters.yml:/counters.yml:ro"
      - "./services/swh-counters/entrypoint.sh:/entrypoint.sh:ro"
    healthcheck:
      test: curl -f http://localhost:5011 || exit 1
      interval: 10s
      timeout: 5s
      retries: 10

  swh-counters-journal-client:
    image: swh/stack
    build: ./
    entrypoint: /entrypoint.sh
    command: journal-client
    environment:
      SWH_CONFIG_FILENAME: /counters_journal_client.yml
    env_file:
      - ./env/common_python.env
    depends_on:
      - kafka
      - redis
      - swh-counters
      - prometheus-statsd-exporter
      - nginx
    volumes:
      - "./conf/counters_journal_client.yml:/counters_journal_client.yml:ro"
      - "./services/swh-counters/entrypoint.sh:/entrypoint.sh:ro"

  # Search related

  swh-search:
    image: swh/stack
    build: ./
    entrypoint: /entrypoint.sh
    command: rpc
    healthcheck:
      test: curl -f http://localhost:5010 || exit 1
      retries: 6
    ports:
      - 5010:5010
    environment:
      SWH_CONFIG_FILENAME: /search.yml
      RPC_PORT: 5010
    env_file:
      - ./env/common_python.env
    depends_on:
      - prometheus-statsd-exporter
    volumes:
      - "./conf/search-memory.yml:/search.yml:ro"
      - "./services/swh-search/entrypoint.sh:/entrypoint.sh:ro"

  swh-search-journal-client-objects:
    image: swh/stack
    build: ./
    entrypoint: /entrypoint.sh
    env_file:
      - ./env/common_python.env
    environment:
      SWH_CONFIG_FILENAME: /search_journal_client.yml
    command: journal-client
    depends_on:
      - kafka
      - swh-search
      - nginx
    volumes:
      - "./conf/search_journal_client_objects.yml:/search_journal_client.yml:ro"
      - "./services/swh-search/entrypoint.sh:/entrypoint.sh:ro"

  swh-search-journal-client-indexed:
    image: swh/stack
    build: ./
    entrypoint: /entrypoint.sh
    command: journal-client
    env_file:
      - ./env/common_python.env
    environment:
      SWH_CONFIG_FILENAME: /search_journal_client.yml
    depends_on:
      - kafka
      - swh-search
      - nginx
    volumes:
      - "./conf/search_journal_client_indexed.yml:/search_journal_client.yml:ro"
      - "./services/swh-search/entrypoint.sh:/entrypoint.sh:ro"

  # GraphQL API

  swh-graphql:
    image: swh/stack
    build: ./
    depends_on:
      - swh-storage
      - prometheus-statsd-exporter
      - nginx
    ports:
      - 5013:5013
    environment:
      SWH_CONFIG_FILENAME: /graphql.yml
      RPC_PORT: 5013
    env_file:
      - ./env/common_python.env
    volumes:
      - "./conf/graphql.yml:/graphql.yml:ro"
      - "./services/swh-graphql/entrypoint.sh:/entrypoint.sh:ro"
    entrypoint: /entrypoint.sh
    command: rpc
    healthcheck:
      test: curl -f http://localhost:5013 || exit 1
      retries: 6

volumes:
  redis-data:
  storage-data:
  objstorage-data:
  kafka-data:
